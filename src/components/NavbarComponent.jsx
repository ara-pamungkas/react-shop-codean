import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShoppingCart } from "@fortawesome/free-solid-svg-icons";
import "./Components.css"


function NavbarComponent(props) {
	return (
		<Navbar id="navbar" expand='lg' className='bg-dark'>
			<Container>
				<Navbar.Brand href='#home' className="h3 fw-bold text-white">Codean</Navbar.Brand>
				<Navbar.Brand href='#home' className="text-white" onClick={props.toggleCart}>
					<FontAwesomeIcon icon={faShoppingCart} />
				</Navbar.Brand>
			</Container>
		</Navbar>
	);
}

export default NavbarComponent;
