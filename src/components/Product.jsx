import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { useNavigate } from "react-router-dom";

function Product(props) {
	const navigate = useNavigate()

	const getNavigate= (id) => {
		navigate(`/product/${id}`, {
			state: {
				id
			}
		})
	}

	return (
		<Card style={{ width: "18rem" }}>
			<Card.Img variant='top' src={props.image} className="p-3" />
			<ListGroup className='list-group-flush'>
				<ListGroup.Item>Store: {props.storeName}</ListGroup.Item>
			</ListGroup>
			<Card.Body>
				<Card.Title>{props.name}</Card.Title>
				<Card.Text>Rp.{props.price}</Card.Text>
			</Card.Body>
			<Card.Footer className="d-flex gap-3">
				<Button variant='success' onClick={() => (
					props.getProductId(props.id)
				)}>Beli</Button>
				<Button
					variant='danger'
					className='text-decoration-none text-white'
					onClick={() => (
					getNavigate(props.id)
				)}
				>
					<FontAwesomeIcon icon={faHeart} />
				</Button>
			</Card.Footer>
		</Card>
	);
}

export default Product;
