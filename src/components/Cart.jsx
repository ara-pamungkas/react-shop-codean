import React from "react";
import "./Components.css";
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

function Cart(props) {
	return (
		<div>
			<Card>
					<Card.Header>{props.storeName}</Card.Header>
					<Card.Body>
						<Card.Title>{props.name}</Card.Title>
						<Card.Text>
							{props.price}
						</Card.Text>
						<Button variant='primary'>Go somewhere</Button>
					</Card.Body>
				</Card>
		</div>
	);
}

export default Cart;
