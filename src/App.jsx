import { BrowserRouter, Routes, Route } from "react-router-dom"
import BodyShop from "./shop/BodyShop"
import SinggleProduct from "./pages/SinggleProduct"

function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route index Component={BodyShop} />
        <Route path="/product/:id" Component={SinggleProduct} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
