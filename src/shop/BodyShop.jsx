import React, { useEffect, useState } from "react";
import Store from "../utils/products.json";
import NavbarComponent from "../components/NavbarComponent";
import Product from "../components/Product";
import Cart from "../components/Cart";

function BodyShop() {
	const [dataStore, setDataStore] = useState([]);

	const [showCart, setShowCart] = useState(false);

	const getProductId = (id) => {
		let index = Store.findIndex((data) => {
			return data.id === id;
		});
		setShowCart(true)
		setDataStore([...dataStore, Store[index]]);
	};

	useEffect(() => {
		console.log(dataStore)
	}, [dataStore])


	const toggleCart = () => {
		setShowCart(reverse => (!reverse) )
	}

	return (
		<div>
			<NavbarComponent toggleCart={toggleCart} />
			<div className={`${showCart? "p-1 col-md-9 justify-content-start" : "col-md-12"} d-flex flex-wrap gap-2 justify-content-center p-2`}>
				{Store.map((data) => (
					<Product {...data} getProductId={getProductId}  />
				))}
			</div>

			{showCart && (
				<div id='cart' className="col-md-3 bg-dark">
					<div className="text-center mb-5">
						<h3 className="text-white">Keranjang Belanja</h3>
					</div>
					<div className="d-flex flex-column gap-3">
					{dataStore.map((data) => (
						<Cart {...data} />
					))}
					</div>
				</div>
			)}
		</div>
	);
}

export default BodyShop;
