import Button from "react-bootstrap/Button";
import products from "../utils/store.json";
import Card from "react-bootstrap/Card";
import "./SinggleProduct.css";
import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";

function SinggleProduct() {
  const location = useLocation();

  const [store, setStore] = useState(undefined);

  useEffect(() => {
    if (!location.state.id) {
      setStore(undefined);
    } else {
      let index = products.findIndex(
        (data) => data.id === location.state.id
      );
      setStore(products[index]);
    }
  }, [location.state.id]);

  return (
    <div id='singgle-product' className='p-3 bg-secondary'>
      <div className='text-center'>
        <h2>Detail Produk</h2>
      </div>

      {store ? (
        <div className='d-flex align-items-center justify-content-center'>
		  
          <Card className='mt-5 w-75'>
            <div id='card' className='d-flex gap-2 p-3'>
              <img src={store.image1} width={200} height={200} />
              <Card.Body>
                <Card.Title className='fs-4'>{store.name}</Card.Title>
                <Card.Text className='fs-4'>{store.price}</Card.Text>
                <Card.Text>
                  <h4>Description :</h4>
                  <p>{store.desc}</p>
                </Card.Text>
              </Card.Body>
            </div>
            <Card.Footer className='d-flex justify-content-end gap-3'>
              <Button
                href='/'
                variant='link'
                className='text-decoration-none text-dark'
              >
                Back
              </Button>
              <Button variant='success'>Beli</Button>
            </Card.Footer>
          </Card>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
}

export default SinggleProduct;
